package com.mibarrio_project;

import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.mibarrio_project.model.Usuario;
import com.mibarrio_project.repository.UsuarioRepository;

@SpringBootTest
class ApiMibarrioPassWTests {

	@Autowired
	private UsuarioRepository usuarioRepository;
	
	@Autowired
	private BCryptPasswordEncoder encoder;
	
	@Test
	public void crearUsuarioTest() {
		Usuario us = new Usuario();
		us.setId(1);
		us.setUsername("Administrador");
		us.setEmail("admin@admin.com");
		us.setPassword(encoder.encode("123"));
		Usuario retorna = usuarioRepository.save(us);
		
		assertTrue(retorna.getPassword().equalsIgnoreCase(us.getPassword()));
		
	}
}
