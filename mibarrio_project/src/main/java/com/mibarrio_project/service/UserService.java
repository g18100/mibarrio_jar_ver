package com.mibarrio_project.service;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.mibarrio_project.dto.UserDto;
import com.mibarrio_project.model.Usuario;

public interface UserService extends UserDetailsService{
	Usuario save(UserDto userDto);
}
